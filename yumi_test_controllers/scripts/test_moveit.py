#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

print("============ Starting tutorial setup")
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',
                anonymous=True)

robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()

print("============ Setting move groups ============")
group_left = moveit_commander.MoveGroupCommander("LeftArm")
group_left.set_max_velocity_scaling_factor(1)
#group_left.set_planner_id("ESTkConfigDefault")

group_right = moveit_commander.MoveGroupCommander("RightArm")
right_hand = moveit_commander.MoveGroupCommander("RightGripper")
#right_hand = moveit_commander.MoveGroupCommander("RightGripper")

display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                               moveit_msgs.msg.DisplayTrajectory, queue_size=10)

print("============ Left arm reference frame: %s ============" % group_left.get_planning_frame())
print("============ Left arm end effector link: %s ============" % group_left.get_end_effector_link())

print("============ Robot Groups: ============")
print(robot.get_group_names())

print("============ Printing robot state ============")
print(robot.get_current_state())
print("============")

print("====== Adding Box ======")
box_pose = geometry_msgs.msg.PoseStamped()
box_pose.header.frame_id = robot.get_planning_frame()
box_pose.pose.position.x = 0.3
box_pose.pose.position.y = -0.295
box_pose.pose.position.z = 0.1
scene.add_box("table", box_pose, (0.1, 0.02, 0.1))

print("============ Generating plan_left ============")
print("=== Setting Join Position ===")
group_variable_values_L = group_left.get_current_joint_values()
print(group_variable_values_L)

# group_variable_values_L[0] = 0.0
# group_variable_values_L[1] = -2.2689
# group_variable_values_L[2] = 2.3534
# group_variable_values_L[3] = 0.6981
# group_variable_values_L[4] = 0.7
# group_variable_values_L[5] = 0.8
# group_variable_values_L[6] = 0.5
# group_left.set_joint_value_target(group_variable_values_L)
#
# group_left.go(group_variable_values_L, True)
# group_left.stop()

print("=== Setting Pose ===")
# pose_goal = geometry_msgs.msg.Pose()
# pose_goal.orientation.w = 1.0
# pose_goal.position.x = 1.0
# pose_goal.position.y = 2.0
# pose_goal.position.z = 3.0
#
# group_left.set_pose_target(pose_goal)
# plan = group_left.plan()
# # It is always good to clear your targets after planning with poses.
# # Note: there is no equivalent function for clear_joint_value_targets()
# group_left.clear_pose_targets()
# plan_left = group_left.plan()
#
# print("=== Setting Path ===")
#
# waypoints = []
# #
# wpose = group_left.get_current_pose().pose
# ori_wpose = group_left.get_current_pose().pose
# print(wpose)
# wpose.position.y += 0.2  # and sideways (y)
# waypoints.append(copy.deepcopy(wpose))
# #
# wpose.position.z += 0.2 # Second move forward/backwards in (x)
# waypoints.append(copy.deepcopy(wpose))
# #
# wpose.position.y += 0.2   # Third move sideways (y)
# waypoints.append(copy.deepcopy(wpose))
#
# wpose.position.z -= 0.2   # Third move sideways (y)
# waypoints.append(copy.deepcopy(wpose))
#
# wpose.position.y = ori_wpose.position.y  # Third move sideways (y)
# waypoints.append(copy.deepcopy(wpose))
#
# # wpose.position.y -= 0.6   # Third move sideways (y)
# # waypoints.append(copy.deepcopy(wpose))
#
# # We want the Cartesian path to be interpolated at a resolution of 1 cm
# # which is why we will specify 0.01 as the eef_step in Cartesian
# # translation.  We will disable the jump threshold by setting it to 0.0,
# # ignoring the check for infeasible jumps in joint space, which is sufficient
# # for this tutorial.
# (plan, fraction) = group_left.compute_cartesian_path(
#                                    waypoints,   # waypoints to follow
#                                    0.01,        # eef_step
#                                    0.0)         # jump_threshold

# display_trajectory = moveit_msgs.msg.DisplayTrajectory()
# display_trajectory.trajectory_start = group_left.get_current_state()
# display_trajectory.trajectory.append(plan)
# # Publish
# display_trajectory_publisher.publish(display_trajectory)
#group_left.execute(plan, True)
# Note: We are just planning, not asking move_group to actually move the robot yet:

#print("============ Waiting while RVIZ displays plan_left... ============")
# rospy.sleep(10)

# print("============ Visualizing plan_left ============")
# display_trajectory = moveit_msgs.msg.DisplayTrajectory()
# display_trajectory.trajectory_start = robot.get_current_state()
# display_trajectory.trajectory.append(plan_left)
# display_trajectory_publisher.publish(display_trajectory)

# print("============ Waiting while plan_left is visualized (again)... ============")
# rospy.sleep(5)

# group_left.go(wait=True)


print("============ Generating plan_right ============")
#move to default location
group_right.set_named_target("Default Right Arm")
group_right.go()

right_hand_joint = right_hand.get_current_joint_values()
right_hand_joint[1] -= 0.02
right_hand.set_joint_value_target(right_hand_joint)
right_hand.go(right_hand_joint, wait=True)


print("====Group variable_values====")
group_variable_values = group_right.get_current_joint_values()
print(group_variable_values)

print("===Set Joint Value ===")
group_variable_values[0] = 0.0
group_variable_values[1] = -2.2689
group_variable_values[2] = -2.4534
group_variable_values[3] = 0.6981
group_variable_values[4] = 0.6981
group_variable_values[5] = 0.0 #Gripper Joint up down
group_variable_values[6] = 1.3  #Gripper BASE

group_right.set_joint_value_target(group_variable_values)

group_right.go(group_variable_values, wait=True)
current_join = group_right.get_current_joint_values()

print(current_join)
current_join[5] = -0.29
group_right.set_joint_value_target(current_join)
group_right.go(wait=True)

print(group_right.get_current_pose())

#hand open
right_hand.set_named_target("RightOpen")
right_hand.go(wait=True)


waypoints = []

wpose= group_right.get_current_pose("yumi_link_7_r").pose
print(wpose)

wpose.position.y -= 0.19
waypoints.append(copy.deepcopy(wpose))

wpose.position.x = 0.25 # Second move forward/backwards in (x)
waypoints.append(copy.deepcopy(wpose))

wpose.position.z = 0.255# Second move forward/backwards in (x)
waypoints.append(copy.deepcopy(wpose))

(plan, fraction) = group_right.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

group_right.execute(plan, wait=True)

print("start to close the gripper")
print("This should be failure because the box will cause the collision")
right_hand.set_named_target("RightClose")
right_hand.go(wait=True)

print("Start again close the gripper")
while True:
    right_hand_joint = right_hand.get_current_joint_values()
    right_hand_joint[0] -= 0.001
    right_hand_joint[1] -= 0.001
    right_hand.set_joint_value_target(right_hand_joint)
    success = right_hand.go(right_hand_joint, wait=True)
    if not success:
        break;

touch_links = robot.get_link_names("RightGripper")
scene.attach_box(group_right.get_end_effector_link(), "table", touch_links=touch_links)

print("picking up")
current_join = group_right.get_current_joint_values()
print(current_join)
current_join[5] = 1.1
group_right.set_joint_value_target(current_join)
group_right.go(wait=True)


print("move to left")
waypoints = []

wpose= group_right.get_current_pose("yumi_link_7_r").pose
print(wpose)

wpose.position.y += 0.2
waypoints.append(copy.deepcopy(wpose))

(plan, fraction) = group_right.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

group_right.execute(plan, wait=True)


print("moving join down")
current_join = group_right.get_current_joint_values()
print(current_join)
current_join[5] = -0.20
group_right.set_joint_value_target(current_join)
group_right.go(wait=True)

#hand open
right_hand.set_named_target("RightOpen")
right_hand.go(wait=True)

scene.remove_attached_object(group_right.get_end_effector_link(), name="table")


print("===Set Joint Value ===")
group_variable_values[0] = 0.0
group_variable_values[1] = -2.2689
group_variable_values[2] = -2.4534
group_variable_values[3] = 0.6981
group_variable_values[4] = 0.6981
group_variable_values[5] = 0.0 #Gripper Joint up down
group_variable_values[6] = 1.3  #Gripper BASE

group_right.set_joint_value_target(group_variable_values)

group_right.go(group_variable_values, wait=True)

#
# print("============ Waiting while RVIZ displays plan_left... ============")
# rospy.sleep(10)
#
#
# print("============ Visualizing plan_right ============")
# display_trajectory = moveit_msgs.msg.DisplayTrajectory()
# print ("=====display_trajectory===")
# print(display_trajectory)
#
# #display_trajectory.trajectory_start = robot.get_current_state()
# #display_trajectory.trajectory.append(plan_right)
# #display_trajectory_publisher.publish(display_trajectory)
#
# print("============ Waiting while plan_left is visualized (again)... ============")
# rospy.sleep(5)
#
# group_right.go(wait=True)


moveit_commander.roscpp_shutdown()
