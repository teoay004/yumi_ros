# Yumi Moveit Setup

System Used:
- Ubuntu 20.04
- ROS - Neotic

## Intalling Moveit: 
sudo apt-get install ros-noetic-moveit

## Creating Workplace
source /opt/ros/"distro"/setup.bash

mkdir -p ~/catkin_ws/src

cd catkin_ws

catkin_make

source devel/setup.bash 
(This setup is really importance, it tell ros your workplace. Each time open for a new terminal in this workplace need to call this line!! If you want to make it easy better to append it into /.bashrc folder) 

cd src

git clone https://gitlab.com/teoay004/yumi_ros.git

## start working on project.
cd ~/catkin_ws

source devel/setup.bash

cd src/yumi_ros

roslaunch yumi_moveit_config demo.launch

### Pyhton controll simulation (open another terminal)

cd ~/catkin_ws

source devel/setup.bash

cd src/yumi_ros

python3 yumi_test_controllers/scripts/test_moveit.py

## Optional: Robot Setting
roslaunch moveit_setup_assistant setup_assistant.launch
1. Click "Edit Existing Moveit" and Select the Yumi_moveit_config folder
2. Click "Load Files"




